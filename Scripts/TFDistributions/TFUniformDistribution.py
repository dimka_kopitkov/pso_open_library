import numpy as np
import tensorflow as tf
import sys
import math
from utils import *


########################################################################


class TFUniformDistribution:
    """
    """

    def __init__(self, low, high, dtype):
        self.low = low
        self.high = high
        self.range = self.high - self.low
        self.dtype = dtype
        self.dtype_np = tfdtype_2_npdtype(self.dtype)
        self.dim = np.shape(low)[0]
        self.log_prob_value = -np.sum(np.log(self.range.astype(np.float128)))
        self.prob_value = np.exp(self.log_prob_value)
        self.log_prob_value = self.log_prob_value.astype(np.float32)
        self.prob_value = self.prob_value.astype(np.float32)


    def sample(self, batch_size):
        samples_tensor = tf.random_uniform([batch_size, self.dim], 0.0, 1.0, self.dtype)
        samples_tensor = (samples_tensor*self.range) + self.low
        samples_tensor = tf.identity(samples_tensor, name="uniform_samples")
        return samples_tensor


    def prob(self, samples_tensor):
        batch_size = tf.shape(samples_tensor)[0]
        
        converted_nan = np.cast[self.dtype_np](np.nan)
        nan_tensor = tf.fill([batch_size, 1], converted_nan)
        #nan_tensor = tf.constant(np.nan, dtype=self.dtype, shape=[batch_size, 1])
        zero_tensor = tf.zeros_like(nan_tensor)
        one_tensor = tf.ones_like(nan_tensor)
        prob_tensor = one_tensor*self.prob_value
        
        has_nans = tf.reduce_any(tf.is_nan(samples_tensor), axis = 1, keepdims = True)
        out_range = tf.logical_or(samples_tensor < self.low, samples_tensor > self.high)
        out_range = tf.reduce_any(out_range, axis = 1, keepdims = True)
        pdf_values = tf.where(has_nans,
                              nan_tensor,
                              tf.where(out_range,
                                       zero_tensor,
                                       prob_tensor))
        
        pdf_values = tf.identity(pdf_values, name="uniform_pdf_values")
        return pdf_values


    def log_prob(self, samples_tensor):
        batch_size = tf.shape(samples_tensor)[0]
        
        converted_nan = np.cast[self.dtype_np](np.nan)
        nan_tensor = tf.fill([batch_size, 1], converted_nan)
        zero_tensor = tf.zeros_like(nan_tensor)
        one_tensor = tf.ones_like(nan_tensor)
        log_prob_tensor = one_tensor*self.log_prob_value
        
        has_nans = tf.reduce_any(tf.is_nan(samples_tensor), axis = 1, keepdims = True)
        out_range = tf.logical_or(samples_tensor < self.low, samples_tensor > self.high)
        out_range = tf.reduce_any(out_range, axis = 1, keepdims = True)
        log_pdf_values = tf.where(has_nans,
                                  nan_tensor,
                                  tf.where(out_range,
                                           zero_tensor,
                                           log_prob_tensor))
        log_pdf_values = tf.identity(log_pdf_values, name="uniform_log_pdf_values")
        return log_pdf_values
        
                
########################################################################


