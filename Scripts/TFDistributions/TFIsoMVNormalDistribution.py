import numpy as np
import tensorflow as tf
import sys
import math
from utils import *


########################################################################


class TFIsoMVNormalDistribution:
    """
    """

    def __init__(self, means, stds):
        self.means = means
        self.stds = stds
        self.dtype = self.means.dtype
        self.dim = np.shape(self.means)[0]
        
        self.rv = tf.distributions.Normal(loc = self.means, scale = self.stds)


    def sample(self, batch_size):
        samples_tensor = self.rv.sample([batch_size])
        return samples_tensor


    def prob(self, samples_tensor):
        log_pdf_values = self.log_prob(samples_tensor)
        pdf_values = tf.exp(log_pdf_values)
        #pdf_values = self.rv.prob(samples_tensor)
        #pdf_values = tf.reduce_prod(pdf_values, axis = 1, keep_dims=True)
        return pdf_values


    def log_prob(self, samples_tensor):
        log_pdf_values = self.rv.log_prob(samples_tensor)
        log_pdf_values = tf.reduce_sum(log_pdf_values, axis = 1, keepdims=True)
        return log_pdf_values

########################################################################


