import time, sys, shutil
import math, os
import numpy as np
from utils import *
import Distributions as ds



# Randomness
np.random.seed(10)


data_dim = 20
dis_class_name = 'ds.Columns20DDistribution'
iterative_sampling = False


def create_dist():
    dist_class = eval(dis_class_name)
    dis = dist_class()
    return dis


def sample_points(N):
    # Create distribution object
    dis = create_dist()
    
    samples, pdf_values = dis.sample_distribution(N)

    return samples, pdf_values 


def calculate_pdf_of_samples(samples):
    dis = create_dist()
    pdf_values = dis.pdf(samples)
    
    return pdf_values

    
# Sample testing dataset
val_size = int(1e5)
val_samples, val_pdf_values = sample_points(val_size)

# Sample training dataset
train_size = int(1e8)
train_samples, train_pdf_values = sample_points(train_size)

# Calculate properties of real samples
joined_samples = np.concatenate([val_samples, train_samples])
data_mins = np.min(joined_samples, axis = 0)
data_maxs = np.max(joined_samples, axis = 0)
data_lengths = data_maxs - data_mins
data_means = np.mean(joined_samples, axis = 0)
data_stds = np.std(joined_samples, axis = 0)

# Sample grid validation data (for visualization)
grid_dim_num = 257;
x1 = np.linspace(-3.0, 3.0, num=grid_dim_num, endpoint=True, dtype = np.float32)
x2 = np.copy(x1)
grid_x1_axes = x1
grid_x2_axes = x2
grid_points = cartesian_product([x1, x2])
x3 = np.zeros([grid_points.shape[0], data_dim - 2], dtype = np.float32)
grid_points = np.concatenate([grid_points, x3], axis = 1)
grid_pdf_values = calculate_pdf_of_samples(grid_points)

samples_file = 'distr_dataset.npz'
content = dict();
content['train_points'] = train_samples
content['train_pdf_values'] = train_pdf_values
content['test_points'] = val_samples
content['test_pdf_values'] = val_pdf_values
content['data_mins'] = data_mins
content['data_maxs'] = data_maxs
content['data_lengths'] = data_lengths
content['data_means'] = data_means
content['data_stds'] = data_stds
content['grid_points'] = grid_points
content['grid_pdf_values'] = grid_pdf_values
content['grid_dim_num'] = grid_dim_num
content['grid_x1_axes'] = grid_x1_axes
content['grid_x2_axes'] = grid_x2_axes
content['dis_class_name'] = dis_class_name
content['data_dim'] = data_dim

np.savez(samples_file, **content)
