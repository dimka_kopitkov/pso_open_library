import numpy as np
import sys
import os
import tensorflow as tf
sys.path.insert(0, './TFDatasets/')
sys.path.insert(0, './Models/')
from NumpyGenerator import *
from GeneratorDataset import *
from PSOModel import *


# Setup configuration
dataset_size = int(1e8)
batch_size = int(1e3)
it_num = 300000
dtype = np.float32
dtype_tf = npdtype_2_tfdtype(dtype)
dataset_path = 'distr_dataset.npz'
lrate = 3.5e-3
beta1 = 0.75


config = dict()
config['d_dist'] = 'uniform'
config['act_func'] = 'tf.nn.leaky_relu'
config['use_bd_nn'] = 'True'
config['fc_size'] = 1024
config['layer_num'] = 6
config['blocks_num'] = 50
config['block_size'] = 64

#config['use_bd_nn'] = 'False'
#config['fc_size'] = 1024
#config['layer_num'] = 4


# Prepare output dir
model_dir = './trained_surfc/'
ensure_path_exists(model_dir)


# Create data pipeline
dt_file = np.load(dataset_path)

dtype_map = dict()
dtype_map['data'] = dtype;
dtype_map['pdf_values'] = dtype;

data_mins = dt_file['data_mins'].astype(dtype)
data_maxs = dt_file['data_maxs'].astype(dtype)
data_lengths = dt_file['data_lengths'].astype(dtype)
data_means = dt_file['data_means'].astype(dtype)
data_stds = dt_file['data_stds'].astype(dtype)

train_map = dict()
train_map['data'] = dt_file['train_points'][0:dataset_size,:]
train_map['pdf_values'] = dt_file['train_pdf_values'][0:dataset_size,:]
gen = NumpyGenerator(train_map, 
                     dtype_map = dtype_map, 
                     batch_size = batch_size, 
                     is_infinite_loop_mode = True);
train_dt = GeneratorDataset(gen)
train_iterator = train_dt.get_dt_object().make_one_shot_iterator()
train_de = train_iterator.get_next()
dt_file.close()


# Create model on gpu for better performance
with tf.device('/gpu:0'):
    # Define model
    de = dict(train_de)
    de['data_mins'] = data_mins
    de['data_maxs'] = data_maxs
    de['data_lengths'] = data_lengths
    de['data_means'] = data_means
    de['data_stds'] = data_stds
    model = PSOModel(de, is_training = True, config = config, dtype = dtype_tf)
    
    # Calculate train LSQR error
    lsqr_tensor = tf.reduce_mean(tf.square(model.log_pdf_tensor_proxy - tf.log(de['pdf_values'])))
    
    # Calculate train IS error
    is_tensor = model.is_error_tensor
    
    # Calculate train NCE error
    nce_tensor = model.nce_error_tensor
    
    # Define learning rate
    global_step = tf.train.get_or_create_global_step()
    lrate_with_decay = tf.train.exponential_decay(
                                        learning_rate = lrate,
                                        global_step = global_step,
                                        decay_steps = 20000,
                                        decay_rate = 0.5,
                                        staircase = True)
    lr_tensor = lrate_with_decay + 3e-9

    # Define an optimizer
    total_loss = tf.losses.get_total_loss()
    optimizer = tf.train.AdamOptimizer(learning_rate = lr_tensor, beta1=beta1, beta2=0.999, epsilon=1e-10)
    total_model_vars = model.myVars
    train_op = optimizer.minimize(total_loss, var_list = total_model_vars, global_step = global_step)


saver = tf.train.Saver(total_model_vars)

with tf.Session() as sess:
    # Intialize the model
    sess.run(tf.global_variables_initializer())
        
    # Push NN surface
    for step in xrange(it_num):
        data = sess.run([train_op, lsqr_tensor])
        curr_lsqr = data[1]
        
        if (step % 100 == 0) or (step == it_num - 1):
            print("Current iteration is: %d" % step)
            print("Current LSQR is: %f" % curr_lsqr)
            
    # Save the variables to disk.
    save_path = saver.save(sess, model_dir + "model.ckpt")
    print("Model saved in path: %s" % save_path)