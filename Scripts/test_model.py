import numpy as np
import sys
import os
import tensorflow as tf
sys.path.insert(0, './TFDatasets/')
sys.path.insert(0, './Models/')
from NumpyGenerator import *
from GeneratorDataset import *
from PSOModel import *


# Setup configuration
batch_size = int(1e3)
dtype = np.float32
dtype_tf = npdtype_2_tfdtype(dtype)
dataset_path = 'distr_dataset.npz'


config = dict()
config['d_dist'] = 'uniform'
config['act_func'] = 'tf.nn.leaky_relu'
config['use_bd_nn'] = 'True'
config['fc_size'] = 1024
config['layer_num'] = 6
config['blocks_num'] = 50
config['block_size'] = 64

#config['use_bd_nn'] = 'False'
#config['fc_size'] = 1024
#config['layer_num'] = 4



# Output dir
model_dir = './trained_surfc/'

# Create data pipeline
dt_file = np.load(dataset_path)

dtype_map = dict()
dtype_map['data'] = dtype;
dtype_map['pdf_values'] = dtype;

data_mins = dt_file['data_mins'].astype(dtype)
data_maxs = dt_file['data_maxs'].astype(dtype)
data_lengths = dt_file['data_lengths'].astype(dtype)
data_means = dt_file['data_means'].astype(dtype)
data_stds = dt_file['data_stds'].astype(dtype)

points_tensor = tf.convert_to_tensor(dt_file['test_points'], dtype = dtype_tf)
pdf_values_tensor = tf.convert_to_tensor(dt_file['test_pdf_values'], dtype = dtype_tf)
dt = tf.data.Dataset.from_tensor_slices({'data' : points_tensor, 'pdf_values' : pdf_values_tensor})
test_dt = dt.batch(batch_size)
iterator = test_dt.make_one_shot_iterator()
next_element = iterator.get_next()

dt_file.close()


# Create model on gpu for better performance
with tf.device('/gpu:0'):
    # Define model
    de = dict(next_element)
    de['data_mins'] = data_mins
    de['data_maxs'] = data_maxs
    de['data_lengths'] = data_lengths
    de['data_means'] = data_means
    de['data_stds'] = data_stds
    model = PSOModel(de, is_training = True, config = config, dtype = dtype_tf)
    total_model_vars = model.myVars
    
    # Calculate LSQR
    lsqr_errors_tensor = tf.square(model.log_pdf_tensor - tf.log(de['pdf_values']))


saver = tf.train.Saver(total_model_vars)

with tf.Session() as sess:
    # Load the model
    saver.restore(sess, model_dir + "model.ckpt")
        
    # Collect test LSQR errors
    gathered_errors = []
    while (True):
        try:
            data = sess.run([lsqr_errors_tensor])
            lsqr_vec = data[0]
            gathered_errors.append(np.squeeze(lsqr_vec))
            print(len(gathered_errors))
        except tf.errors.OutOfRangeError:
            break
        
all_errors = np.concatenate(gathered_errors, axis = 0)
test_lsqr = np.mean(all_errors)
print("Test LSQR is: %f" % test_lsqr)