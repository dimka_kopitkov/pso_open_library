import numpy as np
import sys
import os
import tensorflow as tf
sys.path.insert(0, './TFDatasets/')
sys.path.insert(0, './Models/')
from NumpyGenerator import *
from GeneratorDataset import *
from PSOModel import *
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


# Setup configuration
batch_size = int(1e3)
dtype = np.float32
dtype_tf = npdtype_2_tfdtype(dtype)
dataset_path = 'distr_dataset.npz'


config = dict()
config['d_dist'] = 'uniform'
config['act_func'] = 'tf.nn.leaky_relu'
config['use_bd_nn'] = 'True'
config['fc_size'] = 1024
config['layer_num'] = 6
config['blocks_num'] = 50
config['block_size'] = 64

#config['use_bd_nn'] = 'False'
#config['fc_size'] = 1024
#config['layer_num'] = 4



# Output dir
model_dir = './trained_surfc/'

# Create data pipeline
dt_file = np.load(dataset_path)

dtype_map = dict()
dtype_map['data'] = dtype;
dtype_map['pdf_values'] = dtype;

data_mins = dt_file['data_mins'].astype(dtype)
data_maxs = dt_file['data_maxs'].astype(dtype)
data_lengths = dt_file['data_lengths'].astype(dtype)
data_means = dt_file['data_means'].astype(dtype)
data_stds = dt_file['data_stds'].astype(dtype)

points_tensor = tf.convert_to_tensor(dt_file['grid_points'], dtype = dtype_tf)
pdf_values_tensor = tf.convert_to_tensor(dt_file['grid_pdf_values'], dtype = dtype_tf)
dt = tf.data.Dataset.from_tensor_slices({'data' : points_tensor, 'pdf_values' : pdf_values_tensor})
test_dt = dt.batch(batch_size)
iterator = test_dt.make_one_shot_iterator()
next_element = iterator.get_next()

grid_dims = np.array([dt_file['grid_dim_num'], dt_file['grid_dim_num']])
grid_x1_axes = dt_file['grid_x1_axes']
grid_x2_axes = dt_file['grid_x2_axes']

dt_file.close()


# Create model on gpu for better performance
with tf.device('/gpu:0'):
    # Define model
    de = dict(next_element)
    de['data_mins'] = data_mins
    de['data_maxs'] = data_maxs
    de['data_lengths'] = data_lengths
    de['data_means'] = data_means
    de['data_stds'] = data_stds
    model = PSOModel(de, is_training = True, config = config, dtype = dtype_tf)
    total_model_vars = model.myVars


saver = tf.train.Saver(total_model_vars)

with tf.Session() as sess:
    # Load the model
    saver.restore(sess, model_dir + "model.ckpt")
        
    # Collect test LSQR errors
    est_log_pdfs = []
    while (True):
        try:
            data = sess.run([model.log_pdf_tensor_proxy])
            lpdfs_vec = data[0]
            est_log_pdfs.append(np.squeeze(lpdfs_vec))
            print(len(est_log_pdfs))
        except tf.errors.OutOfRangeError:
            break
        
est_log_pdfs = np.concatenate(est_log_pdfs, axis = 0)
est_log_pdfs = np.reshape(est_log_pdfs, grid_dims)
est_pdfs = np.exp(est_log_pdfs)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
X, Y = np.meshgrid(grid_x1_axes, grid_x2_axes)
ax.plot_surface(X, Y, est_pdfs, rstride=1, cstride=1, 
                    linewidth=2, shade = True,
                    cmap=cm.coolwarm)
plt.xlabel('x1');
plt.ylabel('x2');
plt.show();

