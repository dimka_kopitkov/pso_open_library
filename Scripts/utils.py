import argparse
import numpy as np
import math
import sys
import ntpath
import os
from os import listdir
from os.path import isfile, join
import shutil
from random import shuffle
import tensorflow as tf
from Dense import *


def cartesian_product(arrays):
    la = len(arrays)
    dtype = np.result_type(*arrays)
    arr = np.empty([len(a) for a in arrays] + [la], dtype=dtype)
    for i, a in enumerate(np.ix_(*arrays)):
        arr[...,i] = a
    return arr.reshape(-1, la)


def tfdtype_2_npdtype(dtype):
    if (dtype is None):
        return None
    else:
        return dtype.as_numpy_dtype;


def npdtype_2_tfdtype(dtype):
    if (dtype is None):
        return None
    else:
        return tf.as_dtype(dtype);
    
    
def sort_tensors(tensors):
    def getKey(tnsr):
        n = tnsr.name
        n = n.replace('bias', '2222')
        n = n.replace('kernel', '1111')
        return n
    sorted_tensr_list = sorted(list(tensors), key=getKey)

    return sorted_tensr_list


def fully_connected_layer(inputs,
                        num_outputs,
                        activation_fn,
                        custom_getter = None,
                        weights_initializer=tf.contrib.layers.xavier_initializer(),
                        weights_regularizer=None,
                        weight_norm = False,
                        biases_initializer=tf.zeros_initializer(),
                        biases_regularizer=None,
                        reuse=None,
                        trainable=True,
                        scope=None,
                        use_layer_norm=False,
                        use_batch_norm=False):
    with tf.variable_scope(
        scope,
        'fully_connected', [inputs],
        reuse=reuse,
        custom_getter=custom_getter) as sc:
        inputs = tf.convert_to_tensor(inputs)
        dtype = inputs.dtype.base_dtype
        inp_dim = inputs.shape[-1].value
            
        use_bias = not use_batch_norm and not use_layer_norm and biases_initializer
        layer = Dense(num_outputs,
                      activation=None,
                      weight_norm=weight_norm,
                      use_bias=use_bias,
                      kernel_initializer=weights_initializer,
                      bias_initializer=biases_initializer,
                      kernel_regularizer=weights_regularizer,
                      bias_regularizer=biases_regularizer,
                      activity_regularizer=None,
                      kernel_constraint=None,
                      bias_constraint=None,
                      trainable=trainable,
                      dtype=dtype,
                      _scope=sc,
                      _reuse=reuse)
        u = layer.apply(inputs)
        W = layer.kernel
        b = layer.bias
        var_list = layer.myVars

        # Apply normalizer function / layer.
        if use_layer_norm:
            vars_col = sc.name + '/layer_norm'
            u = tf.contrib.layers.layer_norm(u, reuse=reuse, trainable=trainable, scope=sc.name, variables_collections=[vars_col])
            ln_vars = tf.get_collection(vars_col)
            graph = tf.get_default_graph()
            graph.clear_collection(vars_col)
            var_list = var_list + ln_vars
        elif use_batch_norm:
            bn_layer = tf.layers.BatchNormalization(axis=-1,
                                                    dtype=dtype,
                                                    _reuse=reuse,
                                                    _scope=sc)
            u = bn_layer.apply(u, training=True)
            gamma = bn_layer.gamma
            beta = bn_layer.beta
            var_list = var_list + [gamma, beta]
  

        if activation_fn is not None:
            v = activation_fn(u)
        else:
            v = u
      
    return v, u, W, b, var_list


def block_diagonal_layer(inputs,
                         activation_fn,
                         custom_getter = None,
                         weights_initializer=tf.contrib.layers.xavier_initializer(),
                         use_bias = True,
                         biases_initializer=tf.zeros_initializer(),
                         reuse=None,
                         trainable=True,
                         scope=None,
                         use_batch_norm=False):
    # inputs and outputs have shape [BxNxS] where
    # B - batch dimension, N - channel dimension, S - per-channel bandwith dimension
    
    with tf.variable_scope(
        scope,
        'block_diagonal', [inputs],
        reuse=reuse,
        custom_getter=custom_getter) as sc:
        inputs = tf.convert_to_tensor(inputs)
        dtype = inputs.dtype.base_dtype
        N_dim = inputs.shape[1].value
        S_dim = inputs.shape[2].value
        
        kernel = tf.get_variable('kernel', [N_dim, S_dim, S_dim], dtype=dtype, initializer=weights_initializer, trainable=trainable)
        
        Wx = tf.einsum('ijm,jkm->ijk', inputs, kernel)
        u = Wx
        var_list = [kernel]
        
        if (use_bias):
            bias = tf.get_variable('bias', [N_dim, S_dim], dtype=dtype, initializer=biases_initializer, trainable=trainable)
            u = u + bias
            var_list.append(bias)

        #u = tf.reshape(u, [-1,N_dim*S_dim])
        if (use_batch_norm):
            bn_layer = tf.layers.BatchNormalization(axis=-1,
                                                    dtype=dtype,
                                                    _reuse=reuse,
                                                    _scope=sc)
            u = bn_layer.apply(u, training=True)
            gamma = bn_layer.gamma
            beta = bn_layer.beta
            var_list = var_list + [gamma, beta]

        if activation_fn is not None:
            v = activation_fn(u)
        else:
            v = u
      
    return v, u, var_list


def ensure_path_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)
