import numpy as np
import sys
import math


########################################################################


class BaseDistribution:
    """
    """

    def __init__(self, dim = 1):
        self.dim = dim


    def sample(self, size):
        raise NotImplementedError("Please Implement this method")


    def pdf(self, samples):
        raise NotImplementedError("Please Implement this method")
    
    def sample_distribution(self, samples_num):
        samples = self.sample([samples_num, self.dim])
        pdf_values = self.pdf(samples)
        return samples, pdf_values

    def score_squared_norms(self, samples):
        slopes = self.score(samples)
        return np.sum(np.square(slopes),axis=-1)

                
########################################################################


