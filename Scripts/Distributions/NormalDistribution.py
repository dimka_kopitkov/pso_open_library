import numpy as np
import tensorflow as tf
import sys
import math
from scipy.stats import norm
from BaseDistribution import *


########################################################################


class NormalDistribution(BaseDistribution):
    """
    """

    def __init__(self, mean, std, dim = 1):
        BaseDistribution.__init__(self, dim = dim)
        self.mean = mean
        self.std = std;
        self.inv_var = 1.0/(std*std);
        self.rv = norm(loc=self.mean, scale=self.std);


    def sample(self, size):
        samples = np.random.normal(loc = self.mean, scale=self.std, size=size)
        return samples


    def pdf(self, samples):
        pdf_values = self.rv.pdf(samples)
        
        if (pdf_values.ndim > 1 and pdf_values.shape[1] > 1):
            pdf_values = np.prod(pdf_values, axis=1, keepdims=True)
        
        return pdf_values

                
    def log_pdf(self, samples):
        log_pdf_values = self.rv.logpdf(samples)
        
        if (log_pdf_values.ndim > 1 and log_pdf_values.shape[1] > 1):
            log_pdf_values = np.sum(log_pdf_values, axis=1, keepdims=True)
        
        return log_pdf_values


    def score(self, samples):
        slopes = (samples - self.mean)*(-self.inv_var)

        return slopes

########################################################################


