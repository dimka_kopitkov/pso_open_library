import numpy as np
import tensorflow as tf
import sys
import math
from scipy.stats import norm
from scipy.stats import uniform
from BaseDistribution import *
from UniformDistribution import *
from MVNormalDistribution import *
from MVMixtureDistribution import *


########################################################################


class MVNormalMixtureDistribution(BaseDistribution):
    """
    """

    def __init__(self, mean, cov, weights = None):
        BaseDistribution.__init__(self, dim = mean.shape[1])

        mixed_dists = [MVNormalDistribution(mean = mean[i,:], cov = cov[i,:,:]) for i in xrange(mean.shape[0])]
        self.dis = MVMixtureDistribution(mixed_dists, weights)


    def sample(self, size):
        return self.dis.sample(size)


    def pdf(self, samples):
        return self.dis.pdf(samples)


    def log_pdf(self, samples):
        return self.dis.log_pdf(samples)
                
########################################################################


