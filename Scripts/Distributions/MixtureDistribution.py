import numpy as np
import tensorflow as tf
import sys
import math
from scipy.stats import norm
from scipy.stats import uniform
from BaseDistribution import *
from UniformDistribution import *
from NormalDistribution import *


########################################################################


class MixtureDistribution(BaseDistribution):
    """
    """

    def __init__(self, dist_list, weights):
        BaseDistribution.__init__(self)
        self.dist_list = dist_list
        self.weights = weights
        self.comp_num = len(self.dist_list)


    def sample(self, size):
        # TO-DO: handle weights
        samples_num = size[0]
        cols_num = size[1]
        samples = np.zeros(size, dtype = np.float32);
        comp_choice = np.random.randint(low = 0, high= self.comp_num, size = [samples_num, 1])
        
        for i in range(self.comp_num):
            curr_comp_indeces = np.where(comp_choice == i)[0];
            samples[curr_comp_indeces,:] = self.dist_list[i].sample(size=[curr_comp_indeces.shape[0], cols_num])
            
        return samples


    def pdf(self, samples):
        comp_p = [w*d.pdf(samples) for d, w in zip(self.dist_list, self.weights)];
        comp_p = np.concatenate(comp_p, axis = -1)
        pdf_values = np.sum(comp_p, axis=1, keepdims=True)
        return pdf_values


    def score(self, samples):
        comp_p = [w*d.pdf(samples) for d, w in zip(self.dist_list, self.weights)];
        comp_p = np.concatenate(comp_p, axis = -1)
        pdf_values = np.sum(comp_p, axis=1, keepdims=True)
        comp_score = [d.score(samples) for d, w in zip(self.dist_list, self.weights)];
        comp_score = np.concatenate(comp_score, axis = -1)
        scores = np.sum(comp_p*comp_score, axis=1, keepdims=True)
        slopes = scores/pdf_values

        return slopes

                
########################################################################


