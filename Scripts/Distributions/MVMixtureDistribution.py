import numpy as np
import tensorflow as tf
import sys
import math
from scipy.stats import norm
from scipy.stats import uniform
from BaseDistribution import *
from UniformDistribution import *
from DirichletDistribution import *
from MixtureDistribution import *


########################################################################


class MVMixtureDistribution(BaseDistribution):
    """
    """

    def __init__(self, dists, weights = None):
        BaseDistribution.__init__(self, dim = dists[0].dim)
        self.mixed_dists = dists
        self.dtype = np.float64
        self.comp_num = len(dists)
        
        if (weights is None):
            coef = 1.0/self.comp_num;
            self.weights = np.ones(self.comp_num, dtype = self.dtype)*coef
        else:
            self.weights = weights


    def sample(self, size):
        samples_num = size[0]
        samples_list = [self.mixed_dists[i].sample([samples_num, self.dim]) for i in xrange(self.comp_num)]
       
        choices = np.random.choice(np.array(xrange(self.comp_num)), size=[samples_num, 1], replace=True, p=self.weights)

        # Cleaning data        
        for i in xrange(self.comp_num):
            is_curr = choices == i
            is_curr = is_curr.astype(self.dtype)
            mask = np.repeat(is_curr, self.dim, axis=1)
            samples_list[i] = samples_list[i]*mask

        samples = np.array(samples_list)
        samples = samples.sum(axis=0)
        
        return samples


    def pdf(self, samples):
        samples_p_list = [self.mixed_dists[i].pdf(samples)*self.weights[i] for i in xrange(self.comp_num)]
        samples_p = np.array(samples_p_list)
        pdf_values = samples_p.sum(axis=0)
        return pdf_values


    def log_pdf(self, samples):
        pdf_values = self.pdf(samples)
        log_pdf_values = np.log(pdf_values)
        return log_pdf_values
                
########################################################################


