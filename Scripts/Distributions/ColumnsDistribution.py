import numpy as np
import tensorflow as tf
import sys
import math
from scipy.stats import norm
from scipy.stats import uniform
from BaseDistribution import *
from UniformDistribution import *
from NormalDistribution import *
from MixtureDistribution import *


########################################################################


class ColumnsDistribution(BaseDistribution):
    """
    """

    def __init__(self, dim = 2):
        BaseDistribution.__init__(self, dim = dim)
        
        unif_dist = 0.3;
        unif_center = -2.0;
        d1 = UniformDistribution(low=unif_center-unif_dist, high=unif_center+unif_dist)

        d2 = NormalDistribution(mean = -1.0, std = 0.2)

        d3 = NormalDistribution(mean = 0.0, std = 0.2)

        d4 = NormalDistribution(mean = 1.0, std = 0.2)

        unif_dist = 0.3;
        unif_center = 2.0;
        d5 = UniformDistribution(low=unif_center-unif_dist, high=unif_center+unif_dist)

        comp_num = 5
        coef = 1.0/comp_num;
        weights = [coef, coef, coef, coef, coef]
        dist_list = [d1, d2, d3, d4, d5]

        self.mixed_dist = MixtureDistribution(dist_list, weights)


    def sample(self, size):
        samples_num = size[0]
        samples_list = [self.mixed_dist.sample([samples_num, 1]) for i in range(self.dim)]
        samples = np.concatenate(samples_list, axis = -1)
        #in_range = np.logical_and(samples < 2.3, samples > -2.3)
        #valid_indeces = np.all(in_range, 1).nonzero()[0]
        #samples = samples[valid_indeces,:]
        return samples


    def pdf(self, samples):
        samples_p_list = [self.mixed_dist.pdf(samples[:,i:i+1]) for i in range(self.dim)]
        samples_p = np.concatenate(samples_p_list, axis = -1)
        pdf_values = np.prod(samples_p, axis=1, keepdims=True)
        return pdf_values


    def log_pdf(self, samples):
        samples_p_list = [self.mixed_dist.pdf(samples[:,i:i+1]) for i in range(self.dim)]
        samples_p = np.concatenate(samples_p_list, axis = -1)
        samples_p = np.log(samples_p)
        log_pdf_values = np.sum(samples_p, axis=1, keepdims=True)
        return log_pdf_values
                
                
    def score(self, samples):
        samples_score_list = [self.mixed_dist.score(samples[:,i:i+1]) for i in range(self.dim)]
        slopes = np.concatenate(samples_score_list, axis = -1)

        return slopes


    def score_squared_norms(self, samples):
        slopes = self.score(samples)
        return np.sum(np.square(slopes),axis=-1)


########################################################################


