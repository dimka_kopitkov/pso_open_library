import os

from Columns20DDistribution import *
from UniformDistribution import *
from ColumnsDistribution import *
from Columns20DDistribution import *
from Columns2DDistribution import *
from Columns5DDistribution import *
from Columns10DDistribution import *
from Columns15DDistribution import *
from Columns8DDistribution import *
from Columns12DDistribution import *
from Columns17DDistribution import *
from MVNormalDistribution import *
from MVNormal20Distribution import *
from MVNormal2Distribution import *
from NormalDistribution import *
from TransformedColumnsDistribution import *


# for module in os.listdir(os.path.dirname(__file__)):
#     if module == '__init__.py' or module[-3:] != '.py':
#         continue
#     __import__(module[:-3], locals(), globals())
# del module