import numpy as np
import tensorflow as tf
import sys
import math
from scipy.stats import multivariate_normal
from BaseDistribution import *


########################################################################


class MVNormalDistribution(BaseDistribution):
    """
    """

    def __init__(self, mean, cov):
        BaseDistribution.__init__(self, dim = mean.shape[0])
        self.mean = mean
        self.cov = cov;
        self.inv_cov = np.linalg.inv(cov);
        self.rv = multivariate_normal(mean=self.mean, cov=self.cov);


    def sample(self, size):
        samples = np.random.multivariate_normal(self.mean, self.cov, size=[size[0]])

        return samples


    def pdf(self, samples):
        pdf_values = self.rv.pdf(samples)
        pdf_values = pdf_values[:,np.newaxis]
        
        return pdf_values

    
    def log_pdf(self, samples):
        log_pdf_values = self.rv.logpdf(samples)
        log_pdf_values = log_pdf_values[:, np.newaxis]
        
        return log_pdf_values


    def score(self, samples):
        slopes = -np.matmul(samples - self.mean, self.inv_cov)

        return slopes
    
########################################################################


