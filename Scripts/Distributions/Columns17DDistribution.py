import numpy as np
import tensorflow as tf
import sys
import math
from scipy.stats import norm
from scipy.stats import uniform
from BaseDistribution import *
from TransformedDistribution import *
from ColumnsDistribution import *


########################################################################


class Columns17DDistribution(BaseDistribution):
    """
    """

    def __init__(self):
        BaseDistribution.__init__(self, dim = 17)
        
        self.dis = ColumnsDistribution(dim = 17)


    def sample(self, size):
        return self.dis.sample(size)


    def pdf(self, samples):
        return self.dis.pdf(samples)


    def log_pdf(self, samples):
        return self.dis.log_pdf(samples)


    def score(self, samples):
        return self.dis.score(samples)
    
                
########################################################################


