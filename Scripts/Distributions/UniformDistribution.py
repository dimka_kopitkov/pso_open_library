import numpy as np
import tensorflow as tf
import sys
import math
from BaseDistribution import *


########################################################################


class UniformDistribution(BaseDistribution):
    """
    """

    def __init__(self, low, high, dim = 1):
        BaseDistribution.__init__(self, dim = dim)
        self.low = low
        self.high = high;


    def sample(self, size):
        samples = np.random.uniform(low=self.low, high=self.high, size=size)        
        return samples


    def pdf(self, samples):
        pdf_values = np.ones(samples.shape, dtype = np.float32)
        width = self.high - self.low
        pdf_values = pdf_values * (1/width);
        
        out_indeces = (samples >= self.high).nonzero()[0]
        pdf_values[out_indeces,:] = 0.0
        out_indeces = (samples < self.low).nonzero()[0]
        pdf_values[out_indeces,:] = 0.0

        return pdf_values


    def score(self, samples):
        slopes = np.zeros(samples.shape, dtype = np.float32)

        return slopes
                
########################################################################


