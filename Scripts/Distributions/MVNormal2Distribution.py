import numpy as np
import tensorflow as tf
import sys
import math
from BaseDistribution import *
from MVNormalDistribution import *


########################################################################


class MVNormal2Distribution(BaseDistribution):
    """
    """

    def __init__(self):
        BaseDistribution.__init__(self, dim = 2)
        
        m = np.zeros([self.dim])
        c = np.eye(self.dim)
        self.dis = MVNormalDistribution(mean = m, cov = c)


    def sample(self, size):
        return self.dis.sample(size)


    def pdf(self, samples):
        return self.dis.pdf(samples)


    def log_pdf(self, samples):
        return self.dis.log_pdf(samples)


    def score(self, samples):
        return self.dis.score(samples)

                
########################################################################


