import numpy as np
import tensorflow as tf
import sys
import math
from BaseDistribution import *


########################################################################


class TransformedDistribution(BaseDistribution):
    """
    """

    def __init__(self, dis, A, B = None):
        BaseDistribution.__init__(self, dim = dis.dim)
        self.dis = dis
        self.A = A;
        self.A_inv = np.linalg.inv(self.A)
        self.A_det = np.linalg.det(self.A)
        self.A_det_sign, self.A_logdet = np.linalg.slogdet(self.A)
        self.pdf_coef = np.exp(-self.A_logdet)
        
        if (B is None):
            self.B = np.zeros(self.dim);
        else:
            self.B = B;
        
        self.A_inv_B = np.dot(self.A_inv, self.B)


    def sample(self, size):
        samples_X = self.dis.sample(size)
        samples_Y = np.dot(samples_X, self.A) + self.B

        return samples_Y


    def pdf(self, samples):
        samples_X = np.dot(samples, self.A_inv) - self.A_inv_B
        pdf_values = self.pdf_coef*self.dis.pdf(samples_X)
        
        return pdf_values

    
    def log_pdf(self, samples):
        samples_X = np.dot(samples, self.A_inv) - self.A_inv_B
        log_pdf_values = self.dis.log_pdf(samples_X) - self.A_logdet
        
        return log_pdf_values


    def score(self, samples):
        samples_X = np.dot(samples, self.A_inv) - self.A_inv_B
        scores_X = self.pdf_coef*self.dis.score(samples_X)
        slopes = np.matmul(scores_X, self.A_inv)

        return slopes

                
########################################################################


