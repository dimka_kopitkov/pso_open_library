import math
from math import e
import numpy as np
import tensorflow as tf
import os
import sys
sys.path.insert(0, './TFDistributions/')
from utils import *
from TFUniformDistribution import *
from TFIsoMVNormalDistribution import *


########################################################################


class DSampler:
    """
    """

    def __init__(self, data_element, batch_size, config, dtype):
        self.config = config
        self.dtype = dtype
        self.dtype_np = tfdtype_2_npdtype(self.dtype)
        self.d_dist_name = self.config['d_dist']
        self.sample_num = batch_size
        self.sample_num_float = tf.cast(self.sample_num, self.dtype)
        
        self.data_mins = data_element['data_mins'].astype(self.dtype_np)
        self.data_lengths = data_element['data_lengths'].astype(self.dtype_np)
        self.data_maxs = data_element['data_maxs'].astype(self.dtype_np)
        self.data_stds = data_element['data_stds'].astype(self.dtype_np)
        self.data_means = data_element['data_means'].astype(self.dtype_np)
        self.data_dim = self.data_mins.shape[0]

        self.setup_down_distribution(self.data_mins, self.data_maxs, self.data_means, self.data_stds, data_element)
        self.D_points = self.sample_down_distribution()
        self.D_points_num = self.sample_num


    def setup_down_distribution(self, data_mins, data_maxs, data_means, data_stds, data_element):

        if (self.d_dist_name == 'uniform'):
            self.down_distribution = TFUniformDistribution(data_mins, data_maxs, dtype = self.dtype)
        elif (self.d_dist_name == 'gaussian'):
            self.down_distribution = TFIsoMVNormalDistribution(data_means, data_stds)


    def sample_down_distribution(self, points_num = None):
        if (points_num is None):
            points_num = self.sample_num
            
        points = self.down_distribution.sample(points_num)
        return points;


    def get_down_distribution_log_pdf_values(self, points):
        log_pdf_values = self.down_distribution.log_prob(points)
        return log_pdf_values;
        

    def get_down_distribution_pdf_values(self, points):
        pdf_values = self.down_distribution.prob(points)
        return pdf_values;
    
########################################################################


