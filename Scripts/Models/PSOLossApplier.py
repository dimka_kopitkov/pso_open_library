import math
from math import e
import numpy as np
import tensorflow as tf
slim = tf.contrib.slim
import os
import sys
from tensorflow.python.ops import control_flow_ops
from utils import *


########################################################################


class PSOLossApplier:
    """
    """

    def __init__(self, input_tensors, f_model, is_training, config, dtype):
        
        self.config = config
        self.dtype = dtype
        self.input_tensors = input_tensors
        self.dtype_np = tfdtype_2_npdtype(self.dtype)
        self.is_training = is_training
        self.f_model = f_model;
        
        # Sample down and up points 
        self.D_points = self.input_tensors['D_points']
        self.D_points_num = self.input_tensors['D_points_num']
        self.U_points = self.input_tensors['U_points']
        self.U_points_num = self.input_tensors['U_points_num']

        # Construct loss
        self.create_loss();


    def create_loss(self):

        self.D_func_output = self.f_model.create_model_graph(self.D_points,
                                                             is_training = self.is_training)
        self.D_func_output = tf.identity(self.D_func_output, name="D_func_output")
          
        self.U_func_output = self.f_model.create_model_graph(self.U_points,
                                                             is_training = self.is_training)
        self.U_func_output = tf.identity(self.U_func_output, name="U_func_output")

        
        # Calculate D probability of up points
        self.U_log_pdf_values_from_D_pdf = self.input_tensors['calc_D_log_pdf'](self.U_points)

        # Calculate D probability of down points
        self.D_log_pdf_values_from_D_pdf = self.input_tensors['calc_D_log_pdf'](self.D_points)
        

        # Magnitudes of PSO-LDE        
        alpha = 0.25;
        self.U_anal_force = 1.0/tf.pow(tf.exp(alpha*(self.U_func_output - self.U_log_pdf_values_from_D_pdf)) + 1.0, 1.0/alpha)
        self.D_anal_force = 1.0/tf.pow(tf.exp(alpha*(self.D_log_pdf_values_from_D_pdf - self.D_func_output)) + 1.0, 1.0/alpha)

        self.D_force = tf.stop_gradient(self.D_anal_force)
        self.U_force = tf.stop_gradient(self.U_anal_force)
        
        # Standard PSO loss
        U_mean = tf.reduce_sum(self.U_func_output*self.U_force)/tf.cast(self.U_points_num, self.dtype)
        U_mean = tf.identity(U_mean, name="U_mean")
        D_mean = tf.reduce_sum(self.D_func_output*self.D_force)/tf.cast(self.D_points_num, self.dtype)
        D_mean = tf.identity(D_mean, name="D_mean")
        
        self.pso_loss = - U_mean + D_mean
        tf.losses.add_loss(self.pso_loss)


    def create_pdf_tensors(self, input_tnsr):
        output_tensor = self.f_model.create_model_graph(input_tnsr, 
                                                        is_training = self.is_training)
        
        log_pdf_tensor = output_tensor
        pdf_tensor = tf.exp(log_pdf_tensor)
        
        return [pdf_tensor,
                log_pdf_tensor]
    
########################################################################


