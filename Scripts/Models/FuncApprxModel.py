import math
import numpy as np
import tensorflow as tf
slim = tf.contrib.slim
import os
import sys
from utils import *


########################################################################


class FuncApprxModel:
    """
    """

    def __init__(self, config):
        
        self.config = config
        self.mdlVarsSet = set()
        self.layersSet = set()
        self.hsize = int(self.config['fc_size']);
        self.layers_num = int(self.config['layer_num']);
        self.activation_fn = eval(self.config['act_func'])
        self.blocks_num = int(float(self.config['blocks_num']));
        self.block_size = int(float(self.config['block_size']));
        self.use_block_diagonal_channel = (self.config['use_bd_nn'] == 'True')

        
    def get_list_of_model_variables(self):
        return sort_tensors(self.mdlVarsSet)


    def create_model_graph(self,
                           data_tensor,
                           is_training = True):

        if (self.use_block_diagonal_channel):
            pdf_func = self.create_block_diagonal_network('reg_net', 
                                                        data_tensor, 
                                                        self.block_size,
                                                        self.layers_num,
                                                        1,
                                                        self.activation_fn, 
                                                        final_activ = False,
                                                        trainable=is_training)
        else:
            pdf_func = self.create_fc_network('reg_net', 
                                            data_tensor, 
                                            self.hsize,
                                            self.layers_num,
                                            1,
                                            self.activation_fn, 
                                            final_activ = False,
                                            trainable=is_training)
         
        return pdf_func;


    def create_fc_network(self, 
                           net_name, 
                           input_tensor, 
                           hsize,
                           layers_num,
                           out_dim, 
                           activation_fn,
                           final_activ = False, 
                           trainable=True):
        
        # Xavier initialization
        weights_initializer = tf.contrib.layers.variance_scaling_initializer(factor=1.0, mode='FAN_AVG', uniform=True)


        layer_output = input_tensor
        for i in range(layers_num - 1):
            # Layer i
            space_dim = hsize;
            scope_name = net_name + '/layer' + str(i+1)
            
            v, u, W, b, var_list = fully_connected_layer(layer_output, 
                                               space_dim, 
                                               activation_fn,
                                               weights_initializer = weights_initializer,
                                               scope=scope_name, 
                                               reuse=tf.AUTO_REUSE, 
                                               trainable=trainable)
            self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))
            layer_output = v

        
        # Output for f(X)
        out_layer_dim = out_dim
        out_scope_name = net_name + '/out_layer'
        if (final_activ):
            final_activ_fn = activation_fn
        else:
            final_activ_fn = None
        v, u, W, b, var_list = fully_connected_layer(layer_output, 
                                           out_layer_dim, 
                                           final_activ_fn,
                                           weights_initializer = weights_initializer,
                                           scope=out_scope_name, 
                                           reuse=tf.AUTO_REUSE, 
                                           trainable=trainable)
        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        return v;


    def create_block_diagonal_network(self, 
                                       net_name, 
                                       input_tensor, 
                                       block_size,
                                       layers_num,
                                       out_dim, 
                                       activation_fn,
                                       final_activ = False, 
                                       trainable=True):

        # Number of blocks
        N = self.blocks_num
        FC_hsize = block_size*N
        
        # Xavier initialization
        weights_initializer = tf.contrib.layers.variance_scaling_initializer(factor=2.0, mode='FAN_AVG', uniform=True)
        bias_initializer = tf.zeros_initializer()

        layer_output = input_tensor
        scope_name = net_name + '/layer1'
        v, u, W, b, var_list = fully_connected_layer(layer_output, 
                                           FC_hsize, 
                                           activation_fn,
                                           weights_initializer = weights_initializer,
                                           biases_initializer = bias_initializer,
                                           scope=scope_name, 
                                           reuse=tf.AUTO_REUSE, 
                                           trainable=trainable)

        if (layers_num > 2):
            v = tf.reshape(v, [-1,N,block_size])

        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        # Put in the middle block diagonal layers
        layer_output = v
        for i in range(layers_num - 2):

            scope_name = net_name + '/layer' + str(i+2)
            v, u, var_list = block_diagonal_layer(layer_output,
                        activation_fn,
                        weights_initializer=weights_initializer,
                        use_bias = True,
                        reuse=tf.AUTO_REUSE,
                        trainable=trainable,
                        scope=scope_name)
            
            self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))
            
            if (i == layers_num - 3):
                v = tf.reshape(v, [-1,FC_hsize])
            
            layer_output = v
            
        out_scope_name = net_name + '/out_layer'
        if (final_activ):
            final_activ_fn = activation_fn
        else:
            final_activ_fn = None
            
        v, u, W, b, var_list = fully_connected_layer(layer_output, 
                                           out_dim, 
                                           final_activ_fn,
                                           biases_initializer=bias_initializer,
                                           scope=out_scope_name, 
                                           reuse=tf.AUTO_REUSE, 
                                           trainable=trainable)
        

        self.mdlVarsSet = self.mdlVarsSet.union(set(var_list))

        return v;



########################################################################


