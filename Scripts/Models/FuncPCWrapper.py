import math
import numpy as np
import tensorflow as tf
import os
import sys
from FuncApprxModel import *


########################################################################


class FuncPCWrapper:
    """
    """

    def __init__(self, data_element, config, dtype, calc_D_log_pdf_func):
        
        self.config = config
        self.dtype = dtype
        self.calc_D_log_pdf_func = calc_D_log_pdf_func
        self.dtype_np = tfdtype_2_npdtype(self.dtype)
        self.data_means = data_element['data_means'].astype(self.dtype_np)
        self.data_rstds = 1.0/data_element['data_stds'].astype(self.dtype_np)
        self.f_model = FuncApprxModel(config = self.config)
        self.use_precondition = True

        
    def get_list_of_model_variables(self):
        variables = self.f_model.get_list_of_model_variables()
        return variables


    def create_model_graph(self,
                           data_tensor,
                           is_training = True):
        
        # Pre-conditioning the network
        orig_data_tensor = data_tensor
        if (self.use_precondition):
            data_tensor = (data_tensor - self.data_means)*self.data_rstds
            
        func = self.f_model.create_model_graph(data_tensor,
                                               is_training = is_training)
        
        if (self.use_precondition):
            log_pdf_values_from_D_pdf = self.calc_D_log_pdf_func(orig_data_tensor)
            func = func + log_pdf_values_from_D_pdf
            
        return func;

        
########################################################################
