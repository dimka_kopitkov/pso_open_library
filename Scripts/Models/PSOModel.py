import numpy as np
import tensorflow as tf
import os
import sys
from FuncPCWrapper import *
from PSOLossApplier import *
from DSampler import *


########################################################################


class PSOModel:
    """
    """

    def __init__(self, data_element, is_training, config, dtype = tf.float32):
        
        self.config = config
        self.dtype = dtype
        self.dtype_np = tfdtype_2_npdtype(self.dtype)
        self.is_training = is_training
        self.batch_size = tf.shape(data_element['data'])[0]
        self.batch_size_float = tf.cast(self.batch_size, self.dtype)
        self.data_tensor = data_element['data']
        self.data_mins = data_element['data_mins'].astype(self.dtype_np)
        self.data_lengths = data_element['data_lengths'].astype(self.dtype_np)
        self.data_maxs = data_element['data_maxs'].astype(self.dtype_np)

        self.d_sampler = DSampler(data_element,
                                  self.batch_size,
                                  config = self.config,
                                  dtype = self.dtype)
                
        # Call first time to create all model variables
        self.f_model = FuncPCWrapper(data_element = data_element, 
                                     config = self.config,
                                     dtype = self.dtype,
                                     calc_D_log_pdf_func = self.d_sampler.get_down_distribution_log_pdf_values)

        loss_input_tensors = dict()
        loss_input_tensors['D_points'] = self.d_sampler.D_points
        loss_input_tensors['D_points_num'] = self.d_sampler.D_points_num
        loss_input_tensors['calc_D_log_pdf'] = self.d_sampler.get_down_distribution_log_pdf_values
        loss_input_tensors['U_points'] = self.data_tensor
        loss_input_tensors['U_points_num'] = self.batch_size
        
        self.loss_applier = PSOLossApplier(loss_input_tensors, self.f_model, 
                                           is_training = self.is_training, 
                                           config = self.config,
                                           dtype = self.dtype)
        
        # Create model output tensors for testing
        [self.pdf_tensor, 
         self.log_pdf_tensor] = self.create_pdf_tensors(self.data_tensor)

        [self.pdf_tensor_proxy, 
         self.log_pdf_tensor_proxy] = self.create_pdf_proxy(self.pdf_tensor, 
                                                            self.log_pdf_tensor, 
                                                            self.data_tensor)

        # IS error
        [pdf_tensor_D, 
         log_pdf_tensor_D] = self.create_pdf_tensors(self.d_sampler.D_points)
        [pdf_proxy_tensor_D, 
         log_pdf_proxy_tensor_D] = self.create_pdf_proxy(pdf_tensor_D, 
                                                         log_pdf_tensor_D,
                                                         self.d_sampler.D_points)
                  
        p1 = tf.reduce_sum(self.log_pdf_tensor_proxy)/self.batch_size_float
        D_log_pdf_values_from_D_pdf = self.d_sampler.get_down_distribution_log_pdf_values(self.d_sampler.D_points)
        diff_D = tf.exp(log_pdf_proxy_tensor_D - D_log_pdf_values_from_D_pdf)
        p2 = tf.reduce_sum(diff_D)/self.batch_size_float
        self.is_error_tensor = -p1 + p2
        
        U_log_pdf_values_from_D_pdf = self.d_sampler.get_down_distribution_log_pdf_values(self.data_tensor)
        diff_U = tf.exp(U_log_pdf_values_from_D_pdf - self.log_pdf_tensor_proxy)
        p1 = tf.log(1.0 + diff_U)
        p1 = tf.reduce_sum(p1)/self.batch_size_float
        p2 = tf.log(1.0 + diff_D)
        p2 = tf.reduce_sum(p2)/self.batch_size_float
        self.nce_error_tensor = p1 + p2
        
        # Indicate what variables should be trained
        self.myVars = self.f_model.get_list_of_model_variables()


    def create_pdf_tensors(self, points):
        [pdf_tensor, 
         log_pdf_tensor] = self.loss_applier.create_pdf_tensors(points)
        
        return [pdf_tensor, 
                log_pdf_tensor]


    def create_pdf_proxy(self, pdf_tensor, log_pdf_tensor, points):
        pdf_tensor_squeezed = tf.squeeze(pdf_tensor, axis = -1)
        log_pdf_tensor_squeezed = tf.squeeze(log_pdf_tensor, axis = -1)
        zero_outputs = tf.zeros(tf.shape(pdf_tensor_squeezed), self.dtype)
        ones_outputs = tf.ones(tf.shape(pdf_tensor_squeezed), self.dtype)
        points_pdf_values_from_D_pdf = self.d_sampler.get_down_distribution_pdf_values(points)
        points_pdf_values_from_D_pdf = tf.squeeze(points_pdf_values_from_D_pdf, axis = -1)
        positive_D_pdf = tf.greater(points_pdf_values_from_D_pdf, zero_outputs)
        out_range = tf.logical_or(points < self.data_mins, points > self.data_maxs)
        out_range = tf.reduce_any(out_range, axis = 1)
        in_range = tf.logical_not(out_range)
        is_log_pdf_nan = tf.is_nan(log_pdf_tensor_squeezed)
        is_log_pdf_not_nan = tf.logical_not(is_log_pdf_nan)
        valid_points = in_range
        valid_points = tf.logical_and(valid_points, positive_D_pdf)
        valid_points = tf.logical_and(valid_points, is_log_pdf_not_nan)
        log_pdf_min = -80
        
        pdf_proxy_tensor = pdf_tensor_squeezed
        log_pdf_proxy_tensor = log_pdf_tensor_squeezed
        pdf_proxy_tensor = tf.where(valid_points, x=pdf_proxy_tensor, y=zero_outputs)
        log_pdf_proxy_tensor = tf.where(valid_points, x=log_pdf_proxy_tensor, y=ones_outputs*log_pdf_min)
                        
        pdf_proxy_tensor = tf.expand_dims(pdf_proxy_tensor, -1)
        log_pdf_proxy_tensor = tf.expand_dims(log_pdf_proxy_tensor, -1)
        return pdf_proxy_tensor, log_pdf_proxy_tensor

########################################################################


