# PSO_open_library

Basic code to train NN surfaces via PSO from:
Kopitkov, D., Indelman, V.: General probabilistic surface optimization and Log Density Estimation. arXiv preprint arXiv:1903.10567 (2019)
https://arxiv.org/abs/1903.10567


Instructions:

1. Execute "create_distribution_dataset.py" to create file with samples from P^U
2. Execute "train_model.py" to train model f(X, \theta)
3. Execute "test_model.py" to calculate LSQR test error of the trained NN
4. Execute "draw_surface.py" to draw first two dimensions of the trained NN

Required libraries:
- TensorFlow, was tested on version 1.10

In case of additional questions, please ask them via e-mail:
dimkak@gmail.com
